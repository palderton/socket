# dynav-loomo
Run pytorch pretrained model on loomo

1. Install python3-dev
```
sudo apt-get install python3-dev
```
2. Install all the python dependencies for system python
```
/usr/bin/python3.5 -m pip install [RVO2, dynav]
3. Run compiled binary file
```
PYTHONPATH=. ./cmake-build-debug/dynav-loomo